import TravelDatesPage from "../support/TravelDatesPage"

const carParkBookingUrl = Cypress.env('carParkBookingUrl')

describe('Car Park Booking Scenario', () => {
  it('Car Park Booking', () => {
    cy.visit(carParkBookingUrl)
    cy.get(TravelDatesPage.cookieButtonId).click()
    cy.get(TravelDatesPage.entryDateId).click()
  })
})